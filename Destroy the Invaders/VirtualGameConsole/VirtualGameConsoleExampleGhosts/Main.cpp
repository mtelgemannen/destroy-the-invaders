#include "VGCVirtualGameConsole.h"
#include <string>

using namespace std;

int timer;

class Rectangle{
	int x, y;
	int width, height;
public:

	int getWidth(){
		return width;
	}
	int getHeight(){
		return height;
	}
	int getX(){
		return x;
	}
	int getY(){
		return y;
	}
	void setX(int new_x){
		x = new_x;
	}
	void setY(int new_y){
		y = new_y;
	}
	VGCVector getLocation(){
		return VGCVector(x, y);
	}

	bool intersects(Rectangle other){
		if(((x + width > other.x) && (x < other.x + other.width)) && (y + height > other.y) && (y < other.y + other.height))
			return true;

	}

	Rectangle(int new_x, int new_y, int new_width, int new_height) {
		x = new_x;
		y = new_y;
		width = new_width;
		height = new_height;
	}

	/*void initialize(int new_x, int new_y, int new_width, int new_height){
		x = new_x;
		y = new_y;
		width = new_width;
		height = new_height;
	}*/

	void moveHitBox(VGCVector new_location){
		x = new_location.getX();
		y = new_location.getY();
	}
};

//declaration of all images
VGCImage spaceShip_image;
VGCImage invader_image;
VGCImage explosion_image;
VGCImage bullet_image;

//width and height for the window
const int DISPLAY_WIDTH	 = 400;
const int DISPLAY_HEIGHT = 600;

class Bullet{

private:
	Rectangle hitBox;
	//VGCVector bullet_location;
	VGCVector bullet_speed;
	bool invader_friendly;
	bool alive;
public:
	bool isAlive(){
		return alive;
	}
	void render(){
		if(isAlive())
			VGCDisplay::renderImage(bullet_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));
	}

	Bullet(): hitBox(INT_MAX, INT_MAX, VGCDisplay::getWidth(bullet_image), VGCDisplay::getHeight(bullet_image)) {
		alive = false;
		//int x = INT_MAX;
		//int y = INT_MAX;
		//hitBox = Rectangle(x, y, VGCDisplay::getWidth(bullet_image), VGCDisplay::getHeight(bullet_image));
	}

	void fireBullet(VGCVector bullet_location, bool spaceShip_friendly, VGCVector speed) {
		hitBox.moveHitBox(VGCVector(bullet_location.getX(), bullet_location.getY()));
		invader_friendly = !spaceShip_friendly;
		alive = true;
		bullet_speed = speed;
	}

	void move() {
		hitBox.moveHitBox(bullet_speed + hitBox.getLocation());
	}

};

const int bullets_size = 100;
Bullet bullets[bullets_size];

//class for the invaders
class Invader{

private:
	Rectangle hitBox;
	VGCVector invader_speed;
	//VGCVector invader_location;
	bool dead;
	int timeSinceLastDeath;
	VGCVector explosion_location;
public:

	void setTimeSinceLastDeath(int value){
		timeSinceLastDeath = value;
	}

	//checks if this invader is dead
	bool isDead(){
		return dead;
	}
	//kill this invader
	void kill(){
		dead = true;
		timeSinceLastDeath = 0;
		explosion_location = hitBox.getLocation();
	}

	VGCVector getInvader_location(){
		return hitBox.getLocation();
	}
	//render invader @location
	void render(){
		VGCDisplay::renderImage(invader_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));
		if(timeSinceLastDeath < 10000)
			VGCDisplay::renderImage(explosion_image, VGCVector(), explosion_location, VGCAdjustment(0.0, 0.0));
	}

	Invader(): hitBox(INT_MAX, INT_MAX, VGCDisplay::getWidth(invader_image), VGCDisplay::getHeight(invader_image)) {

		timeSinceLastDeath = INT_MAX;

		dead = false;

		//places them in a random location above the screen
		int x = VGCRandomizer::getInt(1, DISPLAY_WIDTH-1);
		int y = VGCRandomizer::getInt(-500, -hitBox.getHeight());

		hitBox = Rectangle(x, y, VGCDisplay::getWidth(invader_image), VGCDisplay::getHeight(invader_image));

		invader_speed = VGCVector(1, 1);

	}

	//checks for collision with edge of screen, and then moves the invader
	void move() {

		//checks for collision with the right edge of the screen
		if(hitBox.getX() > DISPLAY_WIDTH - hitBox.getWidth()) {
			//moves the invader back inside of the screen
			hitBox.setX(DISPLAY_WIDTH - hitBox.getWidth() - 1);
			//inverts the invaders speed. the invader 'bounces'
			invader_speed.setX(-invader_speed.getX());
		}

		//checks for collision with the left edge of the screen
		if(hitBox.getX() < 0) {
			//moves the invader back inside of the screen
			hitBox.setX(1);
			//inverts the invaders speed. the invader 'bounces'
			invader_speed.setX(-invader_speed.getX());
		}
		//moves the invader
		hitBox.moveHitBox(hitBox.getLocation() + invader_speed);

	}


	/*bool isColliding(VGCVector otherEntity, int other_width, int other_height) {
		return !((otherEntity.getY() + other_height < invader_location.getY()) || 
				 (otherEntity.getY() > invader_location.getY() + invader_height) ||
				 (otherEntity.getX() > invader_location.getX() + invader_width) ||
				 (otherEntity.getX() + other_width < invader_location.getX()));
	}*/

};

//const int invaders_size = 5;
//Invader invaders[invaders_size]; 

struct node {

	Invader data;
	struct node* next;
};

node *current;

class InvaderList {

	node *head;

public:

	InvaderList(Invader firstInvader) {
		head->data = firstInvader;
		head->next = NULL;
	}

	InvaderList() {

	}

	node* getHead() {
		return head;
	}

	void addNode(Invader newInvader) {

		node *newNode = new node;
		newNode->data = newInvader;
		newNode->next = NULL;

		if(head == NULL) {
			head = newNode;
			return;
		}
		node *cur = head;
		while(cur) {
			if(cur->next == NULL) {
				cur->next = newNode;
				return;
			}
		cur = cur->next;
		}
	}

	void removeNode(node *thisOne) {
		if(thisOne == head)
			head->next = head;

		current = head;
		while(current != NULL) {
			if(current->next == thisOne) {
				current->next = thisOne->next;
			}
		}
	}

};

InvaderList invaders;

//class for the player
class SpaceShip {
private:
	Rectangle hitBox;
	int spaceShip_health;
	//VGCVector spaceShip_location;
	int spaceShip_speed[4];
	Bullet ssBullets[20];

	void move() {

		if(hitBox.getX() + hitBox.getWidth() >= DISPLAY_WIDTH){

			hitBox.setX(DISPLAY_WIDTH - hitBox.getWidth());
			spaceShip_speed[1] = 0;

		}

		if(hitBox.getX() <= 0 ){

			hitBox.setX(0);
			spaceShip_speed[3] = 0;

		}

		if(hitBox.getY() + hitBox.getHeight() >= DISPLAY_HEIGHT){

			hitBox.setY(DISPLAY_HEIGHT - hitBox.getHeight());
			spaceShip_speed[2] = 0;

		}

		if(hitBox.getY() <= 0 ){

			hitBox.setY(0);
			spaceShip_speed[0] = 0;

		}

		hitBox.setX(hitBox.getX() + spaceShip_speed[1] - spaceShip_speed[3]);
		hitBox.setY(hitBox.getY() + spaceShip_speed[2] - spaceShip_speed[0]);
		
		/*if(VGCKeyboard::isPressed(VGCKey::ARROW_LEFT_KEY)){
			spaceShip_location.setX(spaceShip_location.getX() - 1);
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_RIGHT_KEY)){
			spaceShip_location.setX(spaceShip_location.getX() + 1);
		}
		
		if(VGCKeyboard::isPressed(VGCKey::ARROW_UP_KEY)){
			spaceShip_location.setY(spaceShip_location.getY() - 1);
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_DOWN_KEY)){
			spaceShip_location.setY(spaceShip_location.getY() + 1);
		}
		if(VGCKeyboard::isPressed(VGCKey::SPACE_KEY)){

		}*/

	}
public:

	void render() {

		VGCDisplay::renderImage(spaceShip_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));
		for(int a = 0 ; a < 20 ; a++){
			ssBullets[a].render();
		}

	}

	void shoot(){
		bullets[0].fireBullet(hitBox.getLocation(), true, VGCVector(-3, -10));
	}

	void initialize() {

		//int spaceShip_width  = VGCDisplay::getWidth(spaceShip_image);
		//int spaceShip_height = VGCDisplay::getHeight(spaceShip_image);

		//VGCVector spaceShip_location = VGCVector(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2);
		hitBox = Rectangle(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2, VGCDisplay::getWidth(spaceShip_image), VGCDisplay::getHeight(spaceShip_image));
		for(int a = 0 ; a < 4 ; a++)
			spaceShip_speed[a] = 0;

	}

	SpaceShip(): hitBox(INT_MAX, INT_MAX, VGCDisplay::getWidth(spaceShip_image), VGCDisplay::getHeight(spaceShip_image)) {

	}

	void readInput() {

		for(int a = 0 ; a < 4 ; a++)
			spaceShip_speed[a] = 0;

		if(VGCKeyboard::isPressed(VGCKey::ARROW_LEFT_KEY)){
			spaceShip_speed[3] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_RIGHT_KEY)){
			spaceShip_speed[1] = 1;
		}
		
		if(VGCKeyboard::isPressed(VGCKey::ARROW_UP_KEY)){
			spaceShip_speed[0] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_DOWN_KEY)){
			spaceShip_speed[2] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::SPACE_KEY)){
			shoot();
		}

		/*if(VGCKeyboard::isPressed(VGCKey::ONE_KEY))
			invaders[0].kill();

		if(VGCKeyboard::isPressed(VGCKey::TWO_KEY))
			invaders[1].kill();

		if(VGCKeyboard::isPressed(VGCKey::THREE_KEY))
			invaders[2].kill();

		if(VGCKeyboard::isPressed(VGCKey::FOUR_KEY))
			invaders[3].kill();

		if(VGCKeyboard::isPressed(VGCKey::FIVE_KEY))
			invaders[4].kill();*/

		move();
	}

};

//declaration of spaceShip
SpaceShip spaceShip;

int VGCMain(const VGCStringVector &arguments){
	timer = 0;
	const string applicationName = "Hello World!";
	VGCVirtualGameConsole::initialize(applicationName, DISPLAY_WIDTH, DISPLAY_HEIGHT);
	VGCRandomizer::initializeRandomizer();

	spaceShip_image = VGCDisplay::openImage("Ship.png", 1, 1);
	invader_image = VGCDisplay::openImage("Invader.png", 1, 1);
	explosion_image = VGCDisplay::openImage("Explosion.png", 1, 1);
	bullet_image = VGCDisplay::openImage("Bullet.png", 1, 1);

	/*for(int a = 0 ; a < invaders_size ; a++) {
		invaders[a].setTimeSinceLastDeath(INT_MAX);
		invaders[a].initialize();
	}*/

	invaders.addNode(Invader());

	spaceShip = SpaceShip();

	while(VGCVirtualGameConsole::beginLoop()) {
		timer++;
		spaceShip.readInput();

		/*for(int a = 0 ; a < invaders_size ; a++) {

			invaders[a].move();

			if(invaders[a].isDead() || invaders[a].getInvader_location().getY() > DISPLAY_HEIGHT){
				invaders[a].initialize();
			}
			

		}*/

		current = invaders.getHead();
		while(current != NULL){
			current->data.move();
			/*if(current->data.isDead() || current->data.getInvader_location().getY() > DISPLAY_HEIGHT)
				invaders.removeNode(current);*/
			current = current->next;
		}

		if(VGCDisplay::beginFrame()) {

			const VGCColor backgroundColor = VGCColor(255, 0, 0, 0);
			VGCDisplay::clear(backgroundColor);

			spaceShip.render();
			
			current = invaders.getHead();
			while(current != NULL){
				current->data.render();
				current = current->next;
			}

			for(int a = 0 ; a < bullets_size ; a++) 
				bullets[a].render();

			VGCDisplay::endFrame();
			
		}

		VGCVirtualGameConsole::endLoop();
		

	}

	//no code below here
	//ingen kod nedanf�r denna rad

	VGCDisplay::closeImage(spaceShip_image);
	VGCDisplay::closeImage(invader_image);
	VGCDisplay::closeImage(explosion_image);
	VGCDisplay::closeImage(bullet_image);


	VGCRandomizer::finalizeRandomizer();
	VGCVirtualGameConsole::finalize();

	return 0;

}
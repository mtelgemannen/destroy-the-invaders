#include "VGCVirtualGameConsole.h"
#include "Drawable.h"

class Drawable {
protected:
	VGCRectangle hitbox;
	VGCVector speed;

	void move() {

		hitbox.setPosition(hitbox.getPosition() + speed);

	}

public:

	VGCRectangle getHitbox() {
		return hitbox;
	}

	bool intersects(Drawable &other) {
		return (((hitbox.getPosition().getX() + hitbox.getWidth() > other.getHitbox().getPosition().getX()) 
			&& (hitbox.getPosition().getX() < other.getHitbox().getPosition().getX() + other.getHitbox().getWidth())) 
			&& (hitbox.getPosition().getY() + hitbox.getHeight() > other.getHitbox().getPosition().getY()) 
			&& (hitbox.getPosition().getY() < other.getHitbox().getPosition().getY() + other.getHitbox().getHeight()));
	}

	virtual void update() {
		move();
	}

};
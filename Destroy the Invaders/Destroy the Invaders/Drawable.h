#ifndef Drawable.h
#define DRAWABLE


//all renderable objects inherit this calss
class Drawable {
protected:
	VGCRectangle hitbox;
	VGCVector speed;
	void move();

public:
		VGCRectangle getHitbox();
		bool intersects(Drawable &other);
		virtual void update();
		
};

#endif
#include "VGCVirtualGameConsole.h"
#include <string>

using namespace std;

int timer;

class Rectangle{
	int x, y;
	int width, height;
public:

	int getWidth(){
		return width;
	}
	int getHeight(){
		return height;
	}
	int getX(){
		return x;
	}
	int getY(){
		return y;
	}
	void setX(int new_x){
		x = new_x;
	}
	void setY(int new_y){
		y = new_y;
	}
	VGCVector getLocation(){
		return VGCVector(x, y);
	}

	bool intersects(Rectangle other){
		if(((x + width > other.x) && (x < other.x + other.width)) && (y + height > other.y) && (y < other.y + other.height))
			return true;
		
else
			return false;

	}

	bool contains(VGCVector point) {

		return(point.getX() > x && point.getX() < width && point.getY() > y && point.getY() < height);

	}

	Rectangle(int new_x, int new_y, int new_width, int new_height) {
		x = new_x;
		y = new_y;
		width = new_width;
		height = new_height;
	}

	Rectangle(){}

	/*void initialize(int new_x, int new_y, int new_width, int new_height){
		x = new_x;
		y = new_y;
		width = new_width;
		height = new_height;
	}*/

	void moveHitBox(VGCVector new_location){
		x = new_location.getX();
		y = new_location.getY();
	}
};

//declaration of all images
VGCImage spaceShip_image;
VGCImage invader_image;
VGCImage explosion_image;
VGCImage bullet_image;

//width and height for the window
const int DISPLAY_WIDTH	 = 400;
const int DISPLAY_HEIGHT = 600;

class Bullet{

private:
	Rectangle hitBox;
	//VGCVector bullet_location;
	VGCVector bullet_speed;
	bool invader_friendly;
	bool alive;

public:
	bool isAlive(){
		return alive;
	}
	void render(){
		if(isAlive())
			VGCDisplay::renderImage(bullet_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));
	}

	void move() {
		hitBox.moveHitBox(bullet_speed + hitBox.getLocation());
	}

	Rectangle getHitBox() {
		return hitBox;
	}

	Bullet(){}

	Bullet(int x, int y, bool spaceShip_friendly, VGCVector speed) {
		int hitBox_width = VGCDisplay::getWidth(bullet_image);
		int hitBox_height = VGCDisplay::getHeight(bullet_image);
		hitBox = Rectangle(x, y, hitBox_width, hitBox_height);

		invader_friendly = !spaceShip_friendly;
		alive = true;
		bullet_speed = speed;
	}

};

//const int bullets_size = 100;
//Bullet bullets[bullets_size];

//class for the invaders
class Invader{

private:
	Rectangle hitBox;
	VGCVector invader_speed;
	//VGCVector invader_location;
	bool dead;
	int timeSinceLastDeath;
	VGCVector explosion_location;
	int timeSinceLastShot;
	int shootingFrequency;

public:

	void setTimeSinceLastDeath(int value){
		timeSinceLastDeath = value;
	}

	//checks if this invader is dead
	bool isDead(){
		return dead;
	}
	//kill this invader
	void kill(){
		dead = true;
		timeSinceLastDeath = 0;
		explosion_location = hitBox.getLocation();
	}

	VGCVector getInvader_location(){
		return hitBox.getLocation();
	}

	Rectangle getHitBox() {
		return hitBox;
	}

	//render invader @location
	void render(){
		VGCDisplay::renderImage(invader_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));
		if(timeSinceLastDeath < 10000)
			VGCDisplay::renderImage(explosion_image, VGCVector(), explosion_location, VGCAdjustment(0.0, 0.0));
	}

	Invader(): hitBox(INT_MAX, INT_MAX, VGCDisplay::getWidth(invader_image), VGCDisplay::getHeight(invader_image)) {

		timeSinceLastDeath = INT_MAX;
		timeSinceLastShot = INT_MAX;
		shootingFrequency = 50;

		dead = false;

		//places them in a random location above the screen
		int x = VGCRandomizer::getInt(1, DISPLAY_WIDTH-1);
		int y = VGCRandomizer::getInt(-500, -hitBox.getHeight());

		hitBox = Rectangle(x, y, VGCDisplay::getWidth(invader_image), VGCDisplay::getHeight(invader_image));

		invader_speed = VGCVector(1, 1);

	}

	//checks for collision with edge of screen, and then moves the invader
	void move() {

		
		if(hitBox.getX() > DISPLAY_WIDTH - hitBox.getWidth()) { //checks for collision with the right edge of the screen

			hitBox.setX(DISPLAY_WIDTH - hitBox.getWidth() - 1); //moves the invader back inside of the screen
			invader_speed.setX(-invader_speed.getX()); //inverts the invaders speed. the invader 'bounces'

		}

		if(hitBox.getX() < 0) { //checks for collision with the left edge of the screen
			
			hitBox.setX(1); //moves the invader back inside of the screen
			invader_speed.setX(-invader_speed.getX()); //inverts the invaders speed. the invader 'bounces'

		}

		//moves the invader
		hitBox.moveHitBox(hitBox.getLocation() + invader_speed);

		timeSinceLastShot++;
		shoot();

	}

	void shoot() {
		if(timeSinceLastShot > shootingFrequency) {
			//invader_bullets.addNode(Bullet(hitBox.getX() + hitBox.getWidth()/2, hitBox.getY() + hitBox.getHeight(), false, VGCVector(0, 4)));
			timeSinceLastShot = 0;
		}
	}

};

//const int invaders_size = 5;
//Invader invaders[invaders_size]; 

struct node {

	Invader data;
	Bullet data2;
	 node* next;
};

node *current;

class ObjectList {

	node *head;

public:

	ObjectList(Invader firstInvader) {
		head->data = firstInvader;
		head->next = NULL;
	}

	ObjectList() {

	}

	node* getHead() {
		return head;
	}

	void addNode(Invader newInvader) {

		node *newNode = new node;
		newNode->data = newInvader;
		newNode->next = NULL;

		if(head == NULL) {
			head = newNode;
			return;
		}
		node *cur = head;
		while(cur) {
			if(cur->next == NULL) {
				cur->next = newNode;
				return;
			}
		cur = cur->next;
		}
	}

	void addNode(Bullet newBullet){
		node *newNode = new node;
		newNode->data2 = newBullet;
		newNode->next = NULL;

		if(head == NULL) {
			head = newNode;
			return;
		}
		node *cur = head;
		while(cur) {
			if(cur->next == NULL) {
				cur->next = newNode;
				return;
			}
		cur = cur->next;
		}
	}

	node* removeNode(node *toBeRemoved) {

		for(current = head ; current != NULL ; current = current->next)
			if(current->next == toBeRemoved) {
     				current->next = current->next->next;
				return current;
			}

	}

	string getLength() {
		int counter = 0;

		for(node* current = getHead() ; current != NULL ; current = current->next)
			counter++;	 

	
		return to_string(counter);
	}

};

ObjectList invaders;
ObjectList spaceShip_bullets;
ObjectList invader_bullets;

//class for the player
class SpaceShip {
private:
	Rectangle hitBox;
	int spaceShip_health;
	//VGCVector spaceShip_location;
	int spaceShip_speed[4];
	int timeSinceLastShot;
	int shootingFrequenzy;

	void move() {

		timeSinceLastShot++;

		if(hitBox.getX() + hitBox.getWidth() >= DISPLAY_WIDTH){

			hitBox.setX(DISPLAY_WIDTH - hitBox.getWidth());
			spaceShip_speed[1] = 0;

		}

		if(hitBox.getX() <= 0 ){

			hitBox.setX(0);
			spaceShip_speed[3] = 0;

		}

		if(hitBox.getY() + hitBox.getHeight() >= DISPLAY_HEIGHT){

			hitBox.setY(DISPLAY_HEIGHT - hitBox.getHeight());
			spaceShip_speed[2] = 0;

		}

		if(hitBox.getY() <= 0 ){

			hitBox.setY(0);
			spaceShip_speed[0] = 0;

		}

		hitBox.setX(hitBox.getX() + spaceShip_speed[1] - spaceShip_speed[3]);
		hitBox.setY(hitBox.getY() + spaceShip_speed[2] - spaceShip_speed[0]);

	}
public:

	void render() {

		VGCDisplay::renderImage(spaceShip_image, VGCVector(), hitBox.getLocation(), VGCAdjustment(0.0, 0.0));

	}

	void shoot(){
		if(timeSinceLastShot > shootingFrequenzy) {
			spaceShip_bullets.addNode(Bullet(hitBox.getX() + hitBox.getWidth()/2, hitBox.getY(), true, VGCVector(0, -4)));
			spaceShip_bullets.addNode(Bullet(hitBox.getX() + hitBox.getWidth()/2, hitBox.getY(), true, VGCVector(-1, -4)));
			spaceShip_bullets.addNode(Bullet(hitBox.getX() + hitBox.getWidth()/2, hitBox.getY(), true, VGCVector(1, -4)));
			timeSinceLastShot = 0;
		}
	}

	/*void initialize() {

		//int spaceShip_width  = VGCDisplay::getWidth(spaceShip_image);
		//int spaceShip_height = VGCDisplay::getHeight(spaceShip_image);

		//VGCVector spaceShip_location = VGCVector(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2);
		hitBox = Rectangle(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2, VGCDisplay::getWidth(spaceShip_image), VGCDisplay::getHeight(spaceShip_image));
		

	}*/

	SpaceShip(): hitBox(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2, VGCDisplay::getWidth(spaceShip_image), VGCDisplay::getHeight(spaceShip_image)) {
		for(int a = 0 ; a < 4 ; a++)
			spaceShip_speed[a] = 0;

		shootingFrequenzy = 20;
		timeSinceLastShot = shootingFrequenzy + 1;

	}

	void readInput() {

		for(int a = 0 ; a < 4 ; a++)
			spaceShip_speed[a] = 0;

		if(VGCKeyboard::isPressed(VGCKey::ARROW_LEFT_KEY)){
			spaceShip_speed[3] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_RIGHT_KEY)){
			spaceShip_speed[1] = 1;
		}
		
		if(VGCKeyboard::isPressed(VGCKey::ARROW_UP_KEY)){
			spaceShip_speed[0] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::ARROW_DOWN_KEY)){
			spaceShip_speed[2] = 1;
		}
		if(VGCKeyboard::isPressed(VGCKey::SPACE_KEY)){
			shoot();
		}

		move();
	}

};

void updateBullets() {

	for(current = spaceShip_bullets.getHead() ; current != NULL ; current = current->next)
		current->data2.move();

	for(current = spaceShip_bullets.getHead() ; current != NULL ; current = current->next) {
		if(!Rectangle(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT).contains(current->data2.getHitBox().getLocation())) {
			current = spaceShip_bullets.removeNode(current);
		}

	}

	for(current = spaceShip_bullets.getHead() ; current != NULL ; current = current->next) {
		for(node* currInv = invaders.getHead() ; currInv != NULL ; currInv = currInv->next) {
			if(currInv->data.getHitBox().contains(current->data2.getHitBox().getLocation())) {
    			current = spaceShip_bullets.removeNode(current);
				currInv->data.kill();
				currInv = invaders.removeNode(currInv);
			}
		}
	}

}

void despawn_invaders() {
	for(node* current = invaders.getHead() ; current != NULL ; current = current->next)
		if(current->data.getHitBox().getY() > DISPLAY_HEIGHT)
			current = invaders.removeNode(current);
}

int VGCMain(const VGCStringVector &arguments){
	timer = 0;
	const string applicationName = "Destroy the invaders!";
	VGCVirtualGameConsole::initialize(applicationName, DISPLAY_WIDTH, DISPLAY_HEIGHT);

	spaceShip_image = VGCDisplay::openImage("Ship.png", 1, 1);
	invader_image = VGCDisplay::openImage("Invader.png", 1, 1);
	explosion_image = VGCDisplay::openImage("Explosion.png", 1, 1);
	bullet_image = VGCDisplay::openImage("Bullet.png", 1, 1);

	/*for(int a = 0 ; a < invaders_size ; a++) {
		invaders[a].setTimeSinceLastDeath(INT_MAX);
		invaders[a].initialize();
	}*/

	invaders.addNode(Invader());

	SpaceShip spaceShip;

	VGCFont textFont = VGCDisplay::openFont("Times New Roman", 20);

	while(VGCVirtualGameConsole::beginLoop()) {
		timer++;
		spaceShip.readInput();

		if(timer%100 == 0)
			invaders.addNode(Invader());

		current = invaders.getHead();
		while(current != NULL){
			current->data.move();
			current = current->next;
		}

		updateBullets();
		despawn_invaders();

		if(VGCDisplay::beginFrame()) {

			const VGCColor backgroundColor = VGCColor(255, 0, 0, 0);
			VGCDisplay::clear(backgroundColor);

			spaceShip.render();
			VGCDisplay::renderString(textFont, std::string("number of enemies: " + invaders.getLength()), VGCColor(255, 255, 255, 255), VGCVector(10, 10), VGCAdjustment());
			VGCDisplay::renderString(textFont, std::string("number of bullets: " + spaceShip_bullets.getLength()), VGCColor(255, 255, 255, 255), VGCVector(10, 25), VGCAdjustment());
			
			current = invaders.getHead();
			while(current != NULL){
				current->data.render();
				current = current->next;
			}
			for(current = spaceShip_bullets.getHead() ; current != NULL ; current = current->next)
				current->data2.render();

			VGCDisplay::endFrame();
			
		}

		VGCVirtualGameConsole::endLoop();
		

	}

	//no code below here
	//ingen kod nedanf�r denna rad

	VGCDisplay::closeImage(spaceShip_image);
	VGCDisplay::closeImage(invader_image);
	VGCDisplay::closeImage(explosion_image);
	VGCDisplay::closeImage(bullet_image);
	VGCDisplay::closeFont(textFont);

	VGCVirtualGameConsole::finalize();

	return 0;

}